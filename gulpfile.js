/* eslint-disable arrow-body-style */

const gulp = require('gulp');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const imagemin = require('gulp-imagemin');
const del = require('del');
const browserSync = require('browser-sync').create();

const prefixerOptions = {
  browsers: ['last 2 versions'],
  cascade: false,
};

const sassOptions = {
  outputStyle: 'compressed',
};

const files = {
  html: 'src/*.html',
  js: [
    'node_modules/jquery/dist/jquery.js',
    'src/**/*.js',
  ],
  scss: [
    'src/**/*.scss',
  ],
};

gulp.task('js', () => {
  return gulp.src(files.js)
    .pipe(sourcemaps.init())
    .pipe(concat('scripts.min.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('public/js'));
});

gulp.task('watch:js', ['js'], () => {
  gulp.watch(files.js, ['js']);
});

gulp.task('css', () => {
  return gulp.src(files.scss)
    .pipe(sourcemaps.init())
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(autoprefixer(prefixerOptions))
    .pipe(concat('styles.min.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('public/css'))
    .pipe(browserSync.stream());
});

gulp.task('watch:css', ['css'], () => {
  gulp.watch(files.scss, ['css']);
});

gulp.task('html', () => {
  return gulp.src(files.html)
    .pipe(gulp.dest('public'))
    .pipe(browserSync.stream());
});

gulp.task('watch:html', ['html'], () => {
  gulp.watch(files.html, ['html']);
});

gulp.task('images', () => {
  return gulp.src('src/images/**/*.+(png|jpg|jpeg|gif|svg)')
    .pipe(imagemin())
    .pipe(gulp.dest('public/images'));
});

gulp.task('fonts', () => {
  return gulp.src('src/fonts/*.*')
    .pipe(gulp.dest('public/fonts'));
});

gulp.task('clean:css', () => {
  del.sync([
    'public/css',
  ]);
});

gulp.task('clean:js', () => {
  del.sync([
    'public/js',
  ]);
});

gulp.task('clean:images', () => {
  del.sync([
    'public/images',
  ]);
});

gulp.task('clean:html', () => {
  del.sync([
    'public/*.html',
  ]);
});

gulp.task('watch', ['watch:css', 'watch:js', 'watch:html'], () => {
  browserSync.init({
    server: {
      baseDir: 'public',
    },
  });
});

gulp.task('clean', [
  'clean:css',
  'clean:js',
  'clean:images',
  'clean:html',
]);

gulp.task('build', [
  'clean',
  'css',
  'js',
  'html',
  'images',
  'fonts',
]);

gulp.task('default', [
  'build',
]);
